$(document).ready(function () {
    $('#alert').hide();

    $('.btn-delete').click(function (e) {
        e.preventDefault();
        if (!confirm('¿Está seguro de eliminar?')) {
            return false;
        }

        var row = $(this).parents('tr');
        var id = row.data('id');
        var form = $('#form-delete');
        var url = form.attr('action').replace(':USER_ID', id);
        var data = form.serialize();

        $('#alert').show();
        row.fadeOut();

        $.post(url, data, function (result) {
            $('#products-total').html(result.total);
            $('#alert').html(result.message);
        }).fail(function () {
            alert("El usuario no fue eliminado");
            $('#alert').html("El usuario no fue eliminado");
            row.show();
        });
    });
});