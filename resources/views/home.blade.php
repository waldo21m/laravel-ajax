@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Panel administrativo</div>

                <div class="panel-body">
                    <p>
                        <span id="products-total">{{ $products->total() }}</span> registros |
                        página {{ $products->currentPage() }}
                        de {{ $products->lastPage() }}
                    </p>

                    <div id="alert" class="alert alert-info">

                    </div>

                    <table class="table table-hover table-striped">
                        <thead>
                        <tr>
                            <th width="20px">ID</th>
                            <th>Nombre del producto</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr data-id="{{ $product->id }}">
                                <td width="20px">{{ $product->id }}</td>
                                <td>{{ $product->name }}</td>
                                <td width="20px">
                                    <form method="POST" action="{{ route('destroyProduct', ':USER_ID') }}" id="form-delete">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                        <button type="submit" class="btn btn-link btn-delete">Eliminar</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $products->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ asset('js/script.js') }}"></script>
@endsection
