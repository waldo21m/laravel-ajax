<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTables([
            'users',
            'products',
        ]);

        $this->call(UsersTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
    }

    public function truncateTables(array $tables)
    {
        //La siguiente sentencia sirve para eliminar la revisión de llaves foráneas en la BDD
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        //Sentencia para eliminar los registros de la tabla
        foreach ($tables as $table) {
            DB::table($table)->truncate();
        }
        //Activar revisión de llaves foráneas
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
